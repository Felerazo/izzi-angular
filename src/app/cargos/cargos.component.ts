import {Component, OnInit} from '@angular/core';
import {Cargo} from '../shared/cargo';
import {CargoService} from '../services/cargo.service';

@Component({
  selector: 'app-cargos',
  templateUrl: './cargos.component.html',
  styleUrls: ['./cargos.component.scss']
})
export class CargosComponent implements OnInit {
  cargos: Cargo[];

  constructor(private cargoService: CargoService) {
  }

  ngOnInit() {
    this.cargoService.getCargos().subscribe(
      (response) => {
        this.cargos = response;
        console.log(this.cargos);
      }
    );
  }

}
