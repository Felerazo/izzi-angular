import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Empleado} from '../shared/empleado';
import {EmpleadoService} from '../services/empleado.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-empleado-agregar',
  templateUrl: './empleado-agregar.component.html',
  styleUrls: ['./empleado-agregar.component.scss']
})
export class EmpleadoAgregarComponent implements OnInit {

  empleadoForm: FormGroup;
  empleado: Empleado;

  constructor(private empleadoService: EmpleadoService, private fb: FormBuilder, private router: Router) {
    this.createForm();
  }
  ngOnInit() {
  }

  createForm() {
    this.empleadoForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      ci: ['', Validators.required],
      telefono: ['', Validators.required],
      celular: ['', Validators.required],
      direccion: ['', Validators.required],
      version: 0
    });
  }
  onSubmit() {
    if (this.empleadoForm.valid) {
      this.empleado = this.empleadoForm.value;
      this.empleadoService.postEmpleado(this.empleado).subscribe(
        (response) => {
          this.router.navigate(['/lista-empleados']);
        }
      );
      this.empleadoForm.reset();
    }
  }

  revert() {
    this.empleadoForm.reset();
    this.router.navigate(['/lista-empleados']);
  }
}
