import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EmpleadoService} from '../services/empleado.service';
import {Empleado} from '../shared/empleado';
import {MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-empleado-editar',
  templateUrl: './empleado-editar.component.html',
  styleUrls: ['./empleado-editar.component.scss']
})
export class EmpleadoEditarComponent implements OnInit {
  id: string;
  empleado: Empleado;
  empleadoForm: FormGroup;
  private sub: any;

  constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder, private empleadoService: EmpleadoService) {
    this.createForm();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['eUid'];
      this.empleadoService.getEmpleadoById(this.id).subscribe(
        (response) => {
          this.empleado = response;
          this.updateForm();
          console.log(this.empleado);
        });
    });
  }

  createForm() {
    this.empleadoForm = this.fb.group({
      id: 0,
      firstName: '',
      lastName: '',
      ci: '',
      telefono: '',
      celular: '',
      direccion: ''
    });
  }

  updateForm() {
    this.empleadoForm = this.fb.group({
      id: this.id,
      firstName: [this.empleado.firstName, Validators.required],
      lastName: [this.empleado.lastName, Validators.required],
      ci: [this.empleado.ci, Validators.required],
      telefono: [this.empleado.telefono, Validators.required],
      celular: [this.empleado.celular, Validators.required],
      direccion: [this.empleado.direccion, Validators.required],
      version: this.empleado.version
    });
  }

  onSubmit() {
    if (this.empleadoForm.valid) {
      this.empleado = this.empleadoForm.value;
      this.empleadoService.putEmpleado(this.empleado).subscribe(
        (response) => {
          this.router.navigate(['/lista-empleados']);
          this.empleadoForm.reset();
        }
      );
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  revert() {
    this.empleadoForm.reset();
    this.router.navigate(['/lista-empleados']);
  }
}
