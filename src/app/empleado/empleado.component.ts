import {Component, OnInit, ViewChild} from '@angular/core';
import {Empleado} from '../shared/empleado';
import {EmpleadoService} from '../services/empleado.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit {
  empleados: MatTableDataSource<Empleado>;
  displayedColumns = ['name', 'jobposition', 'telefono', 'celular', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private empleadoService: EmpleadoService) { }

  ngOnInit() {
    this.empleadoService.getEmpleados().subscribe(
      (response) => {
        this.empleados = new MatTableDataSource(response);
        this.empleados.paginator = this.paginator;
      }
    );
  }

}
