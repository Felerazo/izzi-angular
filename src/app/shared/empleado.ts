export class Empleado {
  id: number;
  firstName: string;
  lastName: string;
  name: string;
  ci: number;
  telefono: number;
  celular: number;
  jobPosition: string;
  jobCode: string;
  direccion: string;
  version: number;
}
