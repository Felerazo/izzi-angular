import {TestBed, inject} from '@angular/core/testing';

import {KvalueService} from './kvalue.service';

describe('KvalueService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KvalueService]
    });
  });

  it('should be created', inject([KvalueService], (service: KvalueService) => {
    expect(service).toBeTruthy();
  }));
});
