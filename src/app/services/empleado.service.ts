import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {baseURL} from '../shared/baseurl';
import {Empleado} from '../shared/empleado';

@Injectable()
export class EmpleadoService {

  constructor(private http: HttpClient) { }

  getEmpleados(): Observable<any> {
    return this.http.get<any>( baseURL + 'employees');
  }

  postEmpleado(empleado: Empleado): Observable<any> {
    return this.http.post(baseURL + 'employees', empleado);
  }

  putEmpleado(empleado: Empleado): Observable<any> {
    return this.http.put(baseURL + 'employees', empleado);
  }

  getEmpleadoById(id: string): Observable<any> {
    return this.http.get(baseURL + 'employees/' + id);
  }
}
