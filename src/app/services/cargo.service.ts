
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {baseURL} from '../shared/baseurl';

import {Cargo} from '../shared/cargo';

@Injectable()
export class CargoService {

  constructor(private http: HttpClient) { }

  getCargos(): Observable<any> {
    return this.http.get<any>( baseURL + 'positions');

  }
}
