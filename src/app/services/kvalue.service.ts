import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {baseURL} from '../shared/baseurl';

@Injectable({
  providedIn: 'root'
})
export class KvalueService {

  constructor(private http: HttpClient) {
  }

  getKVcargos(): Observable<any> {
    return this.http.get<any>(baseURL + 'positions?kv=comp');
  }
}
