import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmpleadoVerComponent} from './empleado-ver.component';

describe('EmpleadoVerComponent', () => {
  let component: EmpleadoVerComponent;
  let fixture: ComponentFixture<EmpleadoVerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmpleadoVerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
