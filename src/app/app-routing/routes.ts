import {Routes} from '@angular/router';

import {EmpleadoComponent} from '../empleado/empleado.component';
import {CargosComponent} from '../cargos/cargos.component';
import {EmpleadoAgregarComponent} from '../empleado-agregar/empleado-agregar.component';
import {EmpleadoEditarComponent} from '../empleado-editar/empleado-editar.component';

export  const  routes: Routes = [
  { path: 'lista-empleados', component: EmpleadoComponent, pathMatch: 'full' },
  {path: 'agregar-empleado', component: EmpleadoAgregarComponent, pathMatch: 'full'},
  {path: 'editar-empleado/:eUid', component: EmpleadoEditarComponent, pathMatch: 'full'},
  { path: 'lista-cargos', component: CargosComponent, pathMatch: 'full' }
];
